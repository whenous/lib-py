from functools import wraps
from .decorator import decorator_adaptor, DecoratorAdaptorMeta
from .typing import F


class Allowed(metaclass=DecoratorAdaptorMeta):

    def __init__(self, *allowed_methods) -> None:
        self.allowed_methods = list(allowed_methods)

    def __call__(self, func: F) -> F:
        @wraps(func)
        def wrapper(request):
            print(getattr(func, '__self__', None), getattr(func, '__cls__', None))
            print(request)
            if request not in self.allowed_methods:
                raise ValueError("Invalid method %s" % request)
            return func(request)
        return wrapper


def allowed(*allowed_methods):
    @decorator_adaptor
    def wrapper(func: F) -> F:
        @wraps(func)
        def wrapped(request):
            if request not in allowed_methods:
                raise ValueError("Invalid method %s" % request)
            return func(request)
        return wrapped
    return wrapper


def test_function():
    @allowed('GET')
    def do(request):
        print('1')

    class Foo:
        @allowed('GET', 'POST')
        def do(self, request):
            print(2)

    class Bar(object):

        @allowed('GET', 'POST')
        @classmethod
        def do(cls, request):
            print(3)

    do('GET')
    foo = Foo()
    foo.do('GET')
    Bar.do('GET')


def test_class():

    @Allowed('GET')
    def do(request):
        return 1

    class Foo:
        @Allowed('GET', 'POST')
        def do(self, request) -> int:
            return 1

    class Bar(object):

        @Allowed('GET', 'POST')
        @classmethod
        def do(cls, request) -> int:
            return 1

    do('GET')
    foo = Foo()
    foo.do('GET')
    Bar.do('GET')

if __name__ == '__main__':
    # test_function()
    test_class()
