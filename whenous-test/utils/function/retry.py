from .retry import Retry, AsyncRetry

MAX_TIMES = 1

class Foo:
    max_times = 3

    def __init__(self):
        self.max_times = 4

    @Retry(max_times=1, delay=0)
    def test(self):
        print('test')
        return None

    @Retry(max_factory=lambda self: self.max_times, delay=0)
    def test2(self):
        print('test2')
        return None

    @Retry(max_factory=lambda cls: cls.max_times, delay=0)
    @classmethod
    def test3(cls):
        print('test3')
        return None

    @Retry(max_times=4)
    @staticmethod
    def test4():
        print('test4')
        return None


class Bar:
    max_times = 3

    def __init__(self):
        self.max_times = 2

    @AsyncRetry(max_times=1, delay=0)
    async def test(self):
        print('async test')
        return None

    @AsyncRetry(max_factory=lambda self: self.max_times, delay=1)
    async def test2(self):
        print('async test2')
        return None

    @AsyncRetry(max_factory=lambda cls: cls.max_times, delay=0)
    @classmethod
    async def test3(cls):
        print('async test3')
        return None

    @AsyncRetry(max_times=4)
    @staticmethod
    async def test4():
        print('async test4')
        return None


def run_foo():
    foo = Foo()
    foo.test()
    foo.test2()
    foo.test3()
    foo.test4()


async def run_bar():
    bar = Bar()
    await bar.test()
    await bar.test2()
    await bar.test3()
    await bar.test4()

if __name__ == '__main__':
    run_foo()

    import asyncio
    asyncio.run(run_bar())


