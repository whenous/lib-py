import unittest
from whenous.spider.fetcher import HttpxFetcher, FetchConfig


class Test(unittest.IsolatedAsyncioTestCase):

    async def asyncSetUp(self) -> None:
        await super().asyncSetUp()
        config = FetchConfig()
        self.fetcher = HttpxFetcher(config)

    # async def asyncTearDown(self) -> None:
    #     await super().asyncTearDown()
    #     await self.fetcher.client.aclose()

    async def test_url(self):
        result = await self.fetcher.fetch('https://www.baidu.com')
        self.assertEqual(result.response.status_code, 200)
