# -*- coding: utf-8 -*-

""" Namespace Setup
https://www.python.org/dev/peps/pep-0420/
https://packaging.python.org/guides/packaging-namespace-packages/
"""

from setuptools import setup, find_namespace_packages

setup(
    name='whenous',
    version='1.2.5',
    packages=find_namespace_packages(exclude=('__pycache__',)),
    license='',
    author='whenous',
    author_email='whenous@163.com',
    description='Whenous Python Library',
    url='whenous.com'
)
