import time
import asyncio
from functools import wraps
from typing import Any, Callable, TypeVar, cast
from whenous.utils.object.decorator import DecoratorAdaptorMeta
from whenous.utils.object.typing import F


__all__ = ("Retry", "AsyncRetry")


SelfCls = TypeVar('SelfCls')
MaxFactory = Callable[[SelfCls], int]
RetryFactory = Callable[[SelfCls, Any], bool]
DelayFactory = Callable[[SelfCls, int], int]


class Retry(metaclass=DecoratorAdaptorMeta):
    """ Retry Decorator
    Usage:
        @Retry(max_times=3)
        def test():
            pass

        class Foo:
            max_times = 3

            def __init__(self):
                self.max_times = 4

            @Retry(max_times=5)
            def test(self):
                pass

            @Retry(max_factory=lambda self: self.max_times)
            def test2(self):
                pass

            @Retry(max_factory=lambda cls: cls.max_times)
            def test3(self):
                pass
    """

    def __init__(self, max_times: int = None, max_factory: MaxFactory = None,
                 delay: int = 0, delay_factory: DelayFactory = None,
                 retry_factory: RetryFactory = None) -> None:
        """
        :param max_times: 重试次数
        :param max_factory: 重试次数的方法
        :param delay: 重试延迟时间, 单位: 秒
        :param delay_factory: 重试延迟的方法
        :param retry_factory: 判断是否需要重试的方法
        """
        assert max_times is not None or max_factory
        self.max_factory: MaxFactory = max_factory or (lambda _: max_times)
        self.retry_factory: RetryFactory = retry_factory or (lambda _, result: bool(result is None))
        self.delay_factory: DelayFactory = delay_factory or (lambda _, index: delay)

    def __call__(self, method: F) -> F:
        """
        :param method:  function/instance method/class method/static method
        """
        @wraps(method)
        def wrapper(*args, **kwargs):
            self_or_cls = getattr(method, '__self__', None) or getattr(method, '__cls__', None)
            result = None
            for idx in range(0, max(1, self.max_factory(self_or_cls))):
                result = method(*args, **kwargs)
                if not self.retry_factory(self_or_cls, result):
                    break
                delay = self.delay_factory(self_or_cls, idx)
                if delay:
                    time.sleep(delay)
            return result
        return cast(F, wrapper)


class AsyncRetry(Retry):

    def __call__(self, method: F) -> F:
        @wraps(method)
        async def wrapper(*args, **kwargs):
            self_or_cls = getattr(method, '__self__', None) or getattr(method, '__cls__', None)
            result = None
            for idx in range(0, max(1, self.max_factory(self_or_cls))):
                result = await method(*args, **kwargs)
                if not self.retry_factory(self_or_cls, result):
                    break
                delay = self.delay_factory(self_or_cls, idx)
                if delay:
                    await asyncio.sleep(delay)
            return result
        return cast(F, wrapper)
