# -*- coding: utf-8 -*-
import io
import datetime
import xlwt
import xlrd
from PIL import Image
from openpyxl import Workbook
from openpyxl.worksheet.worksheet import Worksheet
from openpyxl.drawing.image import Image as ImageWrapper
from openpyxl.drawing.image import _import_image


__all__ = ('SimpleXlsWriter', 'SimpleXlsReader', 'XlsWriter')


def convert_value(v):
    if isinstance(v, bool):
        v = '是' if v else '否'
    elif isinstance(v, datetime.datetime) or isinstance(v, datetime.date):
        v = v.strftime('%Y-%m-%d')
    # if isinstance(v, str):
    #     v = v.decode('utf-8', 'ignore')
    return v


class SimpleXlsWriter(object):
    header_style = xlwt.XFStyle()
    header_style.alignment.horz = xlwt.Formatting.Alignment.HORZ_CENTER
    header_style.alignment.vert = xlwt.Formatting.Alignment.VERT_CENTER

    def __init__(self):
        self.book = xlwt.Workbook()

    def save(self, output_file):
        self.book.save(output_file)

    def save_to_bytes(self):
        bio = io.BytesIO()
        self.book.save(bio)
        return bio.getvalue()

    def add_sheet(self, sheet_name, fields, records):
        """
        新建一个表单
        :param sheet_name: 表单名
        :param fields: 表头
        :param records: 表内容

        表头支持单行和多行表头。其中多行为N维数据
        表头和表内容支持合并单元格, 详情见write_row
        """
        sheet = self.book.add_sheet(sheet_name)
        r_index = 0

        # 处理表头, 支持多行表头
        if isinstance(fields[0], (tuple, list)):
            fields_list = fields
        else:
            fields_list = [fields]

        for fields in fields_list:
            self.write_row(sheet, r_index, fields, self.header_style)
            r_index += 1

        for r in records:
            self.write_row(sheet, r_index, r)
            r_index += 1

    @classmethod
    def write_row(cls, sheet: xlwt.Worksheet, r_index: int, r_data: list, style=xlwt.Style.default_style):
        """
        写入一行数据
        :param sheet: 表单
        :param r_index: 行数(从0开始)
        :param r_data: 行数据

        行数据支持合并单元格, 格式如下:
        [{value, width, height}, {value, style}, value]
        width/height表示单元格的宽高, 不填默认为1
        """
        DEFAULT_SIZE = 1
        i = 0
        for d in r_data:
            if isinstance(d, dict):
                val, width, height = d['value'], d.get(
                    'width', DEFAULT_SIZE), d.get('height', DEFAULT_SIZE)
                style = d.get('style') or style
            else:
                val, width, height = d, DEFAULT_SIZE, DEFAULT_SIZE

            if val is None:
                i += 1
                continue

            if isinstance(val, Image.Image):
                img = cls.load_img(val)
                sheet.insert_bitmap_data(img, r_index, i)
            else:
                sheet.write_merge(
                    r_index,
                    r_index + height - 1,
                    i,
                    i + width - 1,
                    convert_value(val),
                    style
                )

            i += width

    @staticmethod
    def load_img(img: Image.Image) -> bytes:
        r, g, b, _ = img.split()
        img = Image.merge("RGB", (r, g, b))
        bio = io.BytesIO()
        img.save(bio, format='bmp')
        img.close()
        return bio.getvalue()

    @staticmethod
    def get_wrapped_style():
        style = xlwt.Style.XFStyle()
        style.alignment.wrap = xlwt.Formatting.Alignment.WRAP_AT_RIGHT
        return style


class SimpleXlsReader(object):

    def __init__(self, filepath: str = None, content: bytes = None):
        """
        :param filepath: excel文件路径
        :param content: excel文件数据
        """
        self.xls = xlrd.open_workbook(filename=filepath, file_contents=content)

    def read_sheet(self, index: int, header: list = None):
        """
        读取一个工作表格
        :param index: 工作表格索引
        :type index: int
        :param header: 表头
        :type header: list
        :return records: [{'test': 1}]
        """
        sheet = self.xls.sheet_by_index(index)

        header = header or sheet.row_values(0)
        header_len = len(header)
        rs = []
        for idx in range(1, sheet.nrows):
            row_data = sheet.row_values(idx)
            _r = row_data[:header_len]
            r = dict(zip(header, _r))
            rs.append(r)
        return rs

    def read_sheet_array(self, index: int):
        """
        读取一个工作表格
        :param index: 工作表格索引
        :type index: int
        :return records: [[1,2,3],[4,5,6]]
        """
        sheet = self.xls.sheet_by_index(index)

        header = sheet.row_values(0)
        header_len = len(header)
        rs = []
        for idx in range(1, sheet.nrows):
            row_data = sheet.row_values(idx)
            _r = row_data[:header_len]
            rs.append(_r)
        return rs

    def get_sheet_fields(self, index: int):
        """
        获取表格第一行抬头
        """
        sheet = self.xls.sheet_by_index(index)
        return sheet.row_values(0)

    def get_sheet_name(self, index: int):
        """
        获取表格名称
        """
        return self.xls.sheet_names()[index]

    @staticmethod
    def convert_time(dt, format_='%H:%M'):
        return xlrd.xldate.xldate_as_datetime(dt, 0).time().strftime(format_)

    @staticmethod
    def convert_datetime(dt, format_='%Y-%m-%d'):
        return xlrd.xldate.xldate_as_datetime(dt, 0).strftime(format_)


class _Image(ImageWrapper):

    def _data(self):
        """
        Return image data, convert to supported types if necessary
        """
        img = _import_image(self.ref)
        # don't convert these file formats
        fp = io.BytesIO()
        img.save(fp, format=self.format)
        fp.seek(0)

        return fp.read()


class XlsWriter(object):

    def __init__(self):
        self.book = Workbook()
        self.book.remove(self.book.active)

    def save(self, output_file):
        self.book.save(output_file)

    def add_sheet(self, sheet_name: str, fields: list, records: list):
        """
        新建一个表单
        :param sheet_name: 表单名
        :param fields: 表头
        :param records: 表内容

        表头支持单行和多行表头。其中多行为N维数据
        表头和表内容支持合并单元格, 详情见write_row
        """
        sheet = self.book.create_sheet(sheet_name)
        r_index = 1

        # 处理表头, 支持多行表头
        if isinstance(fields[0], (tuple, list)):
            fields_list = fields
        else:
            fields_list = [fields]

        for fields in fields_list:
            self.write_row(sheet, r_index, fields)
            r_index += 1

        for r in records:
            self.write_row(sheet, r_index, r)
            r_index += 1

    @classmethod
    def write_row(cls, sheet: Worksheet, r_index: int, r_data: list):
        """
        写入一行数据
        :param sheet: 表单
        :param r_index: 行数(从0开始)
        :param r_data: 行数据
        """
        DEFAULT_SIZE = 1
        i = 1
        for d in r_data:
            if isinstance(d, dict):
                val, width, height = d['value'], d.get('width', DEFAULT_SIZE), d.get('height', DEFAULT_SIZE)
                # style = d.get('style') or style
            else:
                val, width, height = d, DEFAULT_SIZE, DEFAULT_SIZE

            if val is None:
                i += 1
                continue

            if isinstance(val, Image.Image):
                cell = sheet.cell(row=r_index, column=i)
                sheet.add_image(_Image(val), cell.coordinate)
                _row = sheet.row_dimensions[r_index]
                if not _row.height or _row.height < val.height:
                    _row.height = val.height / 1.25
                _col = sheet.column_dimensions[cell.column_letter]
                if not _col.width or _col.width < val.width:
                    _col.width = val.width / 3
            else:
                sheet.cell(
                    row=r_index,
                    column=i,
                    value=convert_value(val)
                )
                if width > 1 or height > 1:
                    sheet.merge_cells(
                        start_row=r_index,
                        start_column=i,
                        end_row=r_index + height - 1,
                        end_column=i + width - 1
                    )

            i += width

    def save_to_bytes(self):
        bio = io.BytesIO()
        self.book.save(bio)
        return bio.getvalue()
