import io
from docxtpl import DocxTemplate

__all__ = ("render_docx", )


def render_docx(template: bytes, context: dict) -> bytes:
    """ 渲染docx. 注意只能是docx!
    :param template: docx 读取后的内容
    :param context: 填充的内容
    """
    dt = DocxTemplate(io.BytesIO(template))
    dt.render(context)

    ret = io.BytesIO()
    dt.save(ret)
    return ret.getvalue()
