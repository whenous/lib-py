
__all__ = (
    'is_range_intersect'
)


def is_range_intersect(ranges: list, begin=lambda x: x[0], end=lambda x: x[1]) -> bool:
    """ 检测范围是否相交
    原理:
        设 a 为 各个活动持续时间之和
        设 b 为 max(End1,End2,...,EndN) - min(Begin1,Begin2,...,BeginN)
        如果 a > b，说明有交集

    :param ranges: [(1,2), (2,3)]
    :param begin:
    :param end:
    """
    if len(ranges) < 2:
        return False
    sum_all = sum([end(item) - begin(item) for item in ranges])
    min_begin = min(begin(item) for item in ranges)
    max_end = max(end(item) for item in ranges)
    return sum_all > (max_end - min_begin)
