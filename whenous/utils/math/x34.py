import random


__all__ = ('x10_to_x34', 'random_x34')


def x10_to_x34(val, width=None, fillchar='0'):
    """ 34进制转换 (去掉英文I/O, 防止和数字冲突) """
    DEFINE = '0123456789ABCDEFGHJKLMNPQRSTUVWXYZ'
    SIZE = len(DEFINE)  # 34进制

    rs = []
    node = val
    while node >= SIZE:
        rs.append(node % SIZE)
        node = node // SIZE
    if node > 0:
        rs.append(node)
    ret = ''.join([DEFINE[r] for r in reversed(rs)])
    if width and fillchar:
        ret = ret.rjust(width, fillchar)
    return ret


def random_x34(width, fillchar=None):
    DEFINE = '0123456789ABCDEFGHJKLMNPQRSTUVWXYZ'
    SIZE = len(DEFINE)  # 34进制

    start = 1
    end = SIZE ** width - 1
    r = random.randint(start, end)
    return x10_to_x34(r, width, fillchar)
