from typing import Any, Callable, TypeVar

__all__ = ('F', )

F = TypeVar('F', bound=Callable[..., Any])  # Function Type


