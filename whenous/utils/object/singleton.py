# -*- coding: utf-8 -*-


class SingletonMeta(type):
    """
    python Singleton, use as:
    class MyClass(metaclass=SingletonMeta):
        pass
    http://stackoverflow.com/questions/6760685/creating-a-singleton-in-python
    """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super().__call__(*args, **kwargs)
        return cls._instances[cls]

    def get_instance(cls):
        return cls._instances.get(cls, None)

