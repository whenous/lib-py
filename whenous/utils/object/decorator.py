"""
https://stackoverflow.com/questions/1288498/using-the-same-decorator-with-arguments-with-functions-and-methods
"""


__all__ = ('decorator_adaptor', 'DecoratorAdaptorMeta')


class _MethodDecoratorAdaptor:

    def __init__(self, decorator, func):
        self.decorator = decorator
        self.func = func

    def __call__(self, *args, **kwargs):
        return self.decorator(self.func)(*args, **kwargs)

    def __get__(self, instance, owner):
        return self.decorator(self.func.__get__(instance, owner))


def decorator_adaptor(decorator):
    """Allows you to use the same decorator on methods and functions,
    hiding the self argument from the decorator."""
    def adapt(func):
        return _MethodDecoratorAdaptor(decorator, func)
    return adapt


class DecoratorAdaptorMeta(type):

    def __call__(self, *args, **kwds):
        r =  super().__call__(*args, **kwds)
        return decorator_adaptor(r)
