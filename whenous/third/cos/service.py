from typing import Union, IO
from qcloud_cos import CosConfig
from qcloud_cos import CosS3Client


class CosConfigEx(CosConfig):

    def __init__(self, Bucket=None, *args, **kwargs):
        self.bucket = Bucket
        super().__init__(*args, **kwargs)


class CosService:

    def __init__(self, config: CosConfigEx):
        self.config = config
        self.client = CosS3Client(self.config)

    def uri(self, key: str) -> str:
        return self.config.uri(self.config.bucket, key)

    def put(self, key: str, body: Union[bytes, IO], contenttype: str = None) -> dict:
        headers = {}
        if contenttype:
            headers['ContentType'] = contenttype
        return self.client.put_object(
            Bucket=self.config.bucket,
            Key=key,
            Body=body,
            EnableMD5=True,
            **headers
        )

    def get(self, key: str) -> dict:
        return self.client.get_object(
            Bucket=self.config.bucket,
            Key=key
        )

    def exists(self, key: str) -> bool:
        return self.client.object_exists(
            Bucket=self.config.bucket,
            Key=key
        )
