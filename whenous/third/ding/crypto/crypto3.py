import io
import time
import struct
import base64
import hashlib
import binascii
import string
from random import choice
from Crypto.Cipher import AES


"""
https://github.com/open-dingtalk/dingtalk-callback-Crypto/blob/main/DingCallbackCrypto3.py
@param token          钉钉开放平台上，开发者设置的token
@param encodingAesKey 钉钉开放台上，开发者设置的EncodingAESKey
@param corpId         企业自建应用-事件订阅, 使用appKey
                      企业自建应用-注册回调地址, 使用corpId
                      第三方企业应用, 使用suiteKey
"""
class DingCallbackCrypto3:
    def __init__(self, token, encodingAesKey, key):
        self.encodingAesKey = encodingAesKey
        self.key = key
        self.token = token
        self.aesKey = base64.b64decode(self.encodingAesKey + '=')

    ## 生成回调处理完成后的success加密数据
    def encrypt_message(self, content, nonce=None, timestamp=None):
        encryptContent = self.encrypt(content)
        timeStamp = timestamp or str(int(time.time()))
        nonce = nonce or self.generateRandomKey(16)
        sign = self.generateSignature(nonce, timeStamp, self.token, encryptContent)
        return {'msg_signature':sign,'encrypt':encryptContent,'timeStamp':timeStamp,'nonce':nonce}

    ##解密钉钉发送的数据
    def decrypt_encrypt_str(self, signature, timestamp, nonce, content):
        """
        解密
        :param content:
        :return:
        """
        sign = self.generateSignature(nonce, timestamp, self.token, content)
        if signature != sign:
            raise ValueError('signature check error')

        content = base64.decodebytes(content.encode('UTF-8'))  ##钉钉返回的消息体

        iv = self.aesKey[:16]  ##初始向量
        aesDecode = AES.new(self.aesKey, AES.MODE_CBC, iv)
        decodeRes = aesDecode.decrypt(content)
        #pad = int(binascii.hexlify(decodeRes[-1]),16)
        pad = int(decodeRes[-1])
        if pad > 32:
            raise ValueError('Input is not padded or padding is corrupt')
        decodeRes = decodeRes[:-pad]
        l = struct.unpack('!i', decodeRes[16:20])[0]
        ##获取去除初始向量，四位msg长度以及尾部corpid
        nl = len(decodeRes)

        if decodeRes[(20+l):].decode() != self.key:
            raise ValueError('corpId 校验错误')
        return decodeRes[20:(20+l)].decode()

    def encrypt(self, content):
        """
        加密
        :param content:
        :return:
        """
        msg_len = self.length(content)
        _content = ''.join([self.generateRandomKey(16) , msg_len.decode() , content , self.key])
        contentEncode = self.pks7encode(_content)
        iv = self.aesKey[:16]
        aesEncode = AES.new(self.aesKey, AES.MODE_CBC, iv)
        aesEncrypt = aesEncode.encrypt(contentEncode.encode('utf8'))
        return base64.b64encode(aesEncrypt).decode('utf8')

    ### 生成回调返回使用的签名值
    def generateSignature(self, nonce, timestamp, token, msg_encrypt):
        v = msg_encrypt
        signList = ''.join(sorted([nonce, timestamp, token, v]))
        return hashlib.sha1(signList.encode()).hexdigest()


    def length(self, content):
        """
        将msg_len转为符合要求的四位字节长度
        :param content:
        :return:
        """
        l = len(content)
        return struct.pack('>l', l)

    def pks7encode(self, content):
        """
        安装 PKCS#7 标准填充字符串
        :param text: str
        :return: str
        """
        l = len(content)
        output = io.StringIO()
        val = 32 - (l % 32)
        for _ in range(val):
            output.write('%02x' % val)
        # print "pks7encode",content,"pks7encode", val, "pks7encode", output.getvalue()
        return content + binascii.unhexlify(output.getvalue()).decode()

    def pks7decode(self, content):
        nl = len(content)
        val = int(binascii.hexlify(content[-1]), 16)
        if val > 32:
            raise ValueError('Input is not padded or padding is corrupt')

        l = nl - val
        return content[:l]


    def generateRandomKey(self, size,
                          chars=string.ascii_letters + string.ascii_lowercase + string.ascii_uppercase + string.digits):
        """
        生成加密所需要的随机字符串
        :param size:
        :param chars:
        :return:
        """
        return ''.join(choice(chars) for i in range(size))

if __name__ == '__main__':
    # dingCrypto = DingCallbackCrypto3("xxxx", "o1w0aum42yaptlz8alnhwikjd3jenzt9cb9wmzptgus", "suiteKeyxx")
    # t = dingCrypto.getEncryptedMap("{xx:11}")
    # print(t)
    # print(t['encrypt'])
    # s = dingCrypto.getDecryptMsg(t['msg_signature'],t['timeStamp'],t['nonce'],t['encrypt'])
    # print("result:",s)

    # test = DingCallbackCrypto3("mryue", "Yue0EfdN5900c1ce5cf6A152c63DDe1808a60c5ecd7", "ding6ccabc44d2c8d38b");

    # # res = test.getEncryptedMap('{"EventType":"check_url"}')
    # # print(res)
    # text = test.getDecryptMsg("03044561471240d4a14bb09372dfcfd4fd0e40cb", "1608001896814", "WL4PK6yA",
    #                           '0vJiX6vliEpwG3U45CtXqi+m8PXbQRARJ8p8BbDuD1EMTDf0jKpQ79QS93qEk7XHpP6u+oTTrd15NRPvNvmBKyDCYxxOK+HZeKju4yhELOFchzNukR+t8SB/qk4ROMu3');
    # print(text)


    app = DingCallbackCrypto3('ijWeAeoyQ', 'u5j5pWF8oLWhBdYvWExgGOD62CbcecqZWfw4mgleSn1', 'dinge332848b4ffb160335c2f4657eb6378f')
    data = {
        "msg_signature": "979b85bc42d359a6cf1abe377df864c3fcb7e2cf",
        "encrypt": "rHSXDEPpLx3JBTfskqst+wSAgvDnjERtIA9hX/xiYBjWBRoOdbSQs5Z8iVR/z+aAe/zjSNJ65D29R5zFqVP12A==",
        "timeStamp": "1620295262651",
        "nonce": "feVdHsM3cnFmDJul"
    }
    print(app.encrypt_message('success', data['timeStamp'], data['nonce']))

    # signature = 'a2949ffd546c959ab7dd1fb7cbdae9daea026507'
    # timestamp = '1620292301924'
    # nonce = '7WakhMpc'
    # encrypt = 'mswFOt9/g+WayHLHUys1yW9FRp2tYW/HIumLUw5dNt9RJmbFW0SabCKIkM5EFTz4hbn0n2Ub1lj/y/bvoel3c8eNo72sxC0uqyLa9kJvSEX5xZeb2t+ZXa39smLDT7bq'
    # print(app.decrypt_encrypt_str(signature, timestamp, nonce, encrypt))


    # app = DingCallbackCrypto3('123456', '4g5j64qlyl3zvetqxz5jiocdr586fn2zvjpa8zls3ij', 'suite4xxxxxxxxxxxxxxx')
    # signature='5a65ceeef9aab2d149439f82dc191dd6c5cbe2c0'
    # timestamp='1445827045067'
    # nonce='nEXhMP4r'
    # encrypt = '1a3NBxmCFwkCJvfoQ7WhJHB+iX3qHPsc9JbaDznE1i03peOk1LaOQoRz3+nlyGNhwmwJ3vDMG+OzrHMeiZI7gTRWVdUBmfxjZ8Ej23JVYa9VrYeJ5as7XM/ZpulX8NEQis44w53h1qAgnC3PRzM7Zc/D6Ibr0rgUathB6zRHP8PYrfgnNOS9PhSBdHlegK+AGGanfwjXuQ9+0pZcy0w9lQ=='
    # print(app.decrypt_encrypt_str(signature, timestamp, nonce, encrypt))
