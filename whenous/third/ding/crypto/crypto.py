import struct
import socket
import base64
from dingtalk.crypto import DingTalkCrypto
from dingtalk.crypto.base import BasePrpCrypto
from dingtalk.crypto.pkcs7 import PKCS7Encoder
from dingtalk.core.utils import to_text, to_binary, random_string, byte2int
from dingtalk.core.exceptions import InvalidCorpIdOrSuiteKeyException


class PrpCryptoEx(BasePrpCrypto):

    def encrypt(self, text, _id):
        return self._encrypt(text, _id)

    def decrypt(self, text, _id):
        return self._decrypt(text, _id, InvalidCorpIdOrSuiteKeyException)

    def _decrypt(self, text, _id, exception=None):
        text = to_binary(text)
        plain_text = self.cipher.decrypt(base64.b64decode(text))
        padding = byte2int(plain_text[-1])
        content = plain_text[16:-padding]
        xml_length = socket.ntohl(struct.unpack(b'I', content[:4])[0])
        xml_content = to_text(content[4:xml_length + 4])
        # from_id = to_text(content[xml_length + 4:])
        # if from_id and from_id != _id:
        #     exception = exception or Exception
        #     raise exception()
        return xml_content

    def _encrypt(self, text, _id):
        text = to_binary(text)
        tmp_list = []
        tmp_list.append(to_binary(self.get_random_string()))
        length = struct.pack(b'I', socket.htonl(len(text)))
        tmp_list.append(length)
        tmp_list.append(text)
        tmp_list.append(to_binary(_id))

        text = b''.join(tmp_list)
        text = PKCS7Encoder.encode(text)

        ciphertext = to_binary(self.cipher.encrypt(text))
        return base64.b64encode(ciphertext)


class DingTalkCryptoEx(DingTalkCrypto):
    crypto_class = PrpCryptoEx

    def __init__(self, token, encoding_aes_key, corpid_or_suitekey):
        super().__init__(token, encoding_aes_key, corpid_or_suitekey)

    def decrypt_encrypt_str(self, signature, timestamp, nonce, encrypt_str):
        return self._decrypt_encrypt_str(signature, timestamp, nonce, encrypt_str, self.crypto_class)

    def encrypt_message(self, msg, nonce=None, timestamp=None):
        return self._encrypt_message(msg, nonce, timestamp, self.crypto_class)

    def decrypt_message(self, msg, signature, timestamp, nonce):
        return self._decrypt_message(
            msg,
            signature,
            timestamp,
            nonce,
            self.crypto_class
        )
