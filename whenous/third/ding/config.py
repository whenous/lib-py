from enum import Enum
from typing import Optional
from whenous.framework.pydantic.settings import BaseSettings


class StorageEnum(str, Enum):
    MEMORY = 'MEMORY'
    REDIS = 'REDIS'


class DingConfig(BaseSettings):
    corp_id: str
    agent_id: int
    app_key: str
    app_secret: str
    login_key: str = ''
    login_secret: str = ''
    event_token: Optional[str] = None
    event_aes_key: Optional[str] = None

    storage: StorageEnum = StorageEnum.MEMORY

    class Config:
        env_prefix = 'ding_'
