from functools import lru_cache
from typing import Type
from .client_ex import AppKeyClientEx, SNSClientEx
from .config import DingConfig



@lru_cache
def get_client(config_class: Type[DingConfig] = DingConfig) -> AppKeyClientEx:
    config = config_class()
    return AppKeyClientEx(
        corp_id=config.corp_id,
        agent_id=config.agent_id,
        app_key=config.app_key,
        app_secret=config.app_secret,
        token=config.event_token,
        aes_key=config.event_aes_key
    )


@lru_cache
def get_sns_client(config_class: Type[DingConfig] = DingConfig) -> SNSClientEx:
    config = config_class()

    return SNSClientEx(
        corp_id=config.corp_id,
        app_key=config.login_key,
        app_secret=config.login_secret
    )

