from dingtalk.client import AppKeyClient
from dingtalk.crypto import DingTalkCrypto
from .api.sns import SNS
from .api.flow import Flow


class SNSClientEx(AppKeyClient):
    sns = SNS()

    def get_access_token_key(self):
        return "app_key:%s" % self.app_key

    def get_access_token(self):
        return self._request(
            'GET',
            '/sns/gettoken',
            params={'appid': self.app_key, 'appsecret': self.app_secret}
        )

    def get_login_url(self, redirect_uri, state=''):
        url = f'https://oapi.dingtalk.com/connect/qrconnect?appid={self.app_key}&response_type=code&scope=snsapi_login&state={state}&redirect_uri={redirect_uri}'
        return url


class AppKeyClientEx(AppKeyClient):
    flow = Flow()

    def __init__(self, corp_id, app_key, app_secret, token=None, aes_key=None, storage=None, timeout=None,
                 auto_retry=True, agent_id=None):
        self.agent_id = agent_id
        super().__init__(corp_id, app_key, app_secret, token=token, aes_key=aes_key, storage=storage, timeout=timeout, auto_retry=auto_retry)
        self.crypto = DingTalkCrypto(token, aes_key, app_key)  # 此处corpid_or_suitekey，在钉钉文档里错误

