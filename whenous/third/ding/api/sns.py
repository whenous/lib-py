from dingtalk.client.api.base import DingTalkBaseAPI

class SNS(DingTalkBaseAPI):

    def getuserinfo_bycode(self, code):
        """
        通过CODE换取用户身份

        :param code: requestAuthCode接口中获取的CODE
        :return:
        """
        return self._post(
            '/sns/getuserinfo_bycode',
            {'tmp_auth_code': code}
        )