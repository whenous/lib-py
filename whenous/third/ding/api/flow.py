from dingtalk.client.api.base import DingTalkBaseAPI


class Flow(DingTalkBaseAPI):
    class Status:
        NEW = 'NEW'
        RUNNING = 'RUNNING'
        TERMINATED = 'TERMINATED'
        COMPLETED = 'COMPLETED'
        CANCELED = 'CANCELED'

        MAP = {
            NEW: '新创建',
            RUNNING: '审批中',
            TERMINATED: '被终止',
            COMPLETED: '完成',
            CANCELED: '取消'
        }

    class Result:
        AGREE = 'AGREE'
        REFUSE = 'REFUSE'
        NONE = 'NONE'
        REDIRECTED = 'REDIRECTED'

        MAP = {
            AGREE: '同意',
            REFUSE: '拒绝',
            NONE: '',
            REDIRECTED: '转交'
        }

    class OperationType:
        EXECUTE_TASK_NORMAL = 'EXECUTE_TASK_NORMAL'
        EXECUTE_TASK_AGENT = 'EXECUTE_TASK_AGENT'
        APPEND_TASK_BEFORE = 'APPEND_TASK_BEFORE'
        APPEND_TASK_AFTER = 'APPEND_TASK_AFTER'
        REDIRECT_TASK = 'REDIRECT_TASK'
        START_PROCESS_INSTANCE = 'START_PROCESS_INSTANCE'
        TERMINATE_PROCESS_INSTANCE = 'TERMINATE_PROCESS_INSTANCE'
        FINISH_PROCESS_INSTANCE = 'FINISH_PROCESS_INSTANCE'
        ADD_REMARK = 'ADD_REMARK'
        REDIRECT_PROCESS = 'REDIRECT_PROCESS'

        MAP = {
            EXECUTE_TASK_NORMAL: '正常执行任务',
            EXECUTE_TASK_AGENT: '代理人执行任务',
            APPEND_TASK_BEFORE: '前加签任务',
            APPEND_TASK_AFTER: '后加签任务',
            REDIRECT_TASK: '转交任务',
            START_PROCESS_INSTANCE: '发起流程实例',
            TERMINATE_PROCESS_INSTANCE: '终止(撤销)流程实例',
            FINISH_PROCESS_INSTANCE: '结束流程实例',
            ADD_REMARK: '添加评论',
            REDIRECT_PROCESS: '审批退回',
        }

    def create_flow(self, agent_id: int, process_code: str, originator_user_id: str, dept_id: int, approvers_v2: list, form_component_values: list):
        """ 发起审批
        https://developers.dingtalk.com/document/app/initiate-approval

        :param agent_id: 应用标识
        :param process_code: 审批流模版code
        :param originator_user_id: 发起人的userid
        :param dep_id: 部门id
        :param approvers_v2: 审批人列表, [{'task_action_type': 'AND|OR|NONE', 'user_ids': []}]
        :param form_component_values: 内容
        """
        data = {
            'agent_id': agent_id,
            'process_code': process_code,
            'originator_user_id': originator_user_id,
            'dept_id': dept_id,
            'approvers_v2': approvers_v2,
            'form_component_values': form_component_values
        }
        return self._post('/topapi/processinstance/create', data=data, result_processor=lambda x: x['process_instance_id'])

    def get_flow(self, process_instance_id: str):
        """ 获取审批实例详情
        https://developers.dingtalk.com/document/app/obtains-the-details-of-a-single-approval-instance

        :param process_instance_id: 审批实例ID
        """
        data = {
            'process_instance_id': process_instance_id
        }
        return self._post('/topapi/processinstance/get', data=data, result_processor=lambda x: x['process_instance'])

    def terminate_flow(self, process_instance_id: str, is_system: bool = False, remark: str = '', operating_userid: int = None):
        """ 终止审批流程
        https://developers.dingtalk.com/document/app/terminate-a-workflow-by-using-an-instance-id

        :param process_instance_id: 审批实例ID
        :param is_system: 是否通过系统操作
        :param remark: 说明
        :param operating_userid: 操作人的userid; 当is_system为false时，该参数必传
        """
        if is_system:
            assert bool(operating_userid)
        data = {
            'request': {
                'process_instance_id': process_instance_id,
                'is_system': is_system,
                'remark': remark,
                'operating_userid': operating_userid
            }
        }
        return self._post('/topapi/process/instance/terminate', data=data)

    def execute_flow(self, process_instance_id: str, remark: str, result: bool, actioner_userid: str, task_id: str):
        """ 执行审批
        https://developers.dingtalk.com/document/app/execute-approval-operation-with-attachment

        :param process_instance_id: 审批实例ID
        :param result: True通过, False拒绝
        :param remark: 说明
        :param actioner_userid: 操作人的userid
        :param task_id: 任务节点id
        """
        _result = 'agree' if result else 'refuse'
        data = {
            'request': {
                'process_instance_id': process_instance_id,
                'result': _result,
                'remark': remark,
                'actioner_userid': actioner_userid,
                'task_id': task_id
            }
        }
        return self._post('/topapi/process/instance/execute', data=data)


    def get_form_field(self, agent_id: int, process_code: str):
        """ 查询已设置为条件的表单组件
        https://developers.dingtalk.com/document/app/query-form-components-that-have-been-set-as-criteria

        :param agent_id: 应用标识
        :param process_code: 审批流模版code
        """
        data = {
            'request': {
                'agent_id': agent_id,
                'process_code': process_code,
            }
        }
        return self._post('/topapi/process/form/condition/list', data=data)

    def get_space(self, user_id: str, agent_id: str = None):
        """ 获取审批钉盘空间信息 (调用本接口获取审批钉盘空间的ID并授予当前用户上传附件的权限)
        https://developers.dingtalk.com/document/app/query-the-space-of-an-approval-nail

        :param user_id: 用户dingid
        """
        data = {
            'user_id': user_id,
        }
        if agent_id:
            data['agent_id'] = agent_id
        return self._post('/topapi/processinstance/cspace/info', data=data, result_processor=lambda x: x['result']['space_id'])

    def get_file(self, process_instance_id: str, file_id: str):
        """ 下载审批附件
        https://developers.dingtalk.com/document/app/grants-the-permission-to-download-the-approval-file

        :param process_instance_id: 审批实例ID
        :param file_id: 文件ID
        """
        data = {
            'request': {
                'process_instance_id': process_instance_id,
                'file_id': file_id
            }
        }
        return self._post('/topapi/processinstance/file/url/get', data=data)
