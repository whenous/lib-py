import os
import enum
import logging
from pathlib import Path
from functools import lru_cache
from typing import ClassVar, Tuple, Type, TypeVar, Union, Optional
from pydantic import BaseSettings as _BaseSettings
from pydantic.env_settings import (
    SettingsSourceCallable,
    EnvSettingsSource
)


__all__ = ('BaseSettings', )


_logger = logging.getLogger(__name__)


class Env(str, enum.Enum):
    LOCAL = 'local'
    DEV = 'dev'
    STAGING = 'staging'
    PROD = 'prod'

    @staticmethod
    def file(env):
        return f'.env.{env}'


class AutoEnvSettingsSource(EnvSettingsSource):
    ENV_FILES = tuple(Env.file(env) for env in Env) + ('.env',)

    def __init__(self, env_file: Union[Path, str, None], env_file_encoding: Optional[str]):
        if not env_file:
            env_file = self.detect_env_file()
        super().__init__(env_file, env_file_encoding)

    def __path_exists(self, path):
        p = Path(path).expanduser()
        if p.is_file():
            _logger.info(f'Env file found: {path}')
            return True
        return False

    def detect_env_file(self):
        env = os.getenv('ENV', None)
        if env:
            f = Env.file(env)
            if self.__path_exists(f):
                return f

        for f in self.ENV_FILES:
            if self.__path_exists(f):
                return f
        return None


S = TypeVar('S')


class BaseSettings(_BaseSettings):
    _cache: ClassVar[S] = None

    class Config:

        @classmethod
        def customise_sources(
            cls,
            init_settings: SettingsSourceCallable,
            env_settings: SettingsSourceCallable,
            file_secret_settings: SettingsSourceCallable,
        ) -> Tuple[SettingsSourceCallable, ...]:
            return init_settings, \
            AutoEnvSettingsSource(env_settings.env_file, env_settings.env_file_encoding), \
            file_secret_settings

    @classmethod
    def cache(cls: Type[S]) -> S:
        if not cls._cache:
            cls._cache = cls()
        return cls._cache


