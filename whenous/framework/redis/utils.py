
def parse_nodes(rs):
    """
    :param rs: 192.168.0.10:6001,192.168.0.11:6001
    """
    if isinstance(rs, str):
        ret = []
        for r in rs.split(','):
            ip, port = r.strip().split(':')
            ret.append((ip, int(port)))
        return ret
    return rs

