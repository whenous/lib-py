import aioredis
from .config import RedisConfig, RedisClientMode


__all__ = ('get_client', )


async def get_standalone_client(config: RedisConfig) -> aioredis.Redis:
    return await aioredis.create_redis_pool(config.url)


async def get_sentinel_client(config: RedisConfig) -> aioredis.Redis:
    sentinel = await aioredis.create_sentinel(config.get_urls())
    redis = sentinel.master_for(config.sentinel_master)
    return redis


async def get_cluster_client(config: RedisConfig) -> aioredis.Redis:
    import aioredis_cluster
    redis = await aioredis_cluster.create_redis_cluster(config.get_urls())
    return redis


async def get_client(config: RedisConfig) -> aioredis.Redis:
    mp = {
        RedisClientMode.STANDALONE: get_standalone_client,
        RedisClientMode.SENTINEL: get_sentinel_client,
        RedisClientMode.CLUSTER: get_cluster_client,
    }
    return await mp[config.mode](config)

