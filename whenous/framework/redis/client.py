import redis
from .config import RedisConfig, RedisClientMode


__all__ = ('get_client', )


def get_standalone_client(config: RedisConfig) -> redis.Redis:
    return redis.Redis.from_url(config.url)


def get_sentinel_client(config: RedisConfig) -> redis.Redis:
    import redis.sentinel
    sentinels = config.get_sentinels()
    sentinel_master = config.sentinel_master

    sentinel = redis.sentinel.Sentinel(sentinels)
    return sentinel.master_for(sentinel_master)


def get_cluster_client(config: RedisConfig) -> redis.Redis:
    from rediscluster import RedisCluster
    clusters = config.get_clusters()
    return RedisCluster(startup_nodes=clusters)


def get_client(config: RedisConfig) -> redis.Redis:
    mp = {
        RedisClientMode.STANDALONE: get_standalone_client,
        RedisClientMode.SENTINEL: get_sentinel_client,
        RedisClientMode.CLUSTER: get_cluster_client,
    }
    return mp[config.mode](config)

