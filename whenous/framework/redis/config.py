import enum
from pydantic import BaseSettings
from .utils import parse_nodes


class RedisClientMode(str, enum.Enum):
    STANDALONE = 'standalone'
    SENTINEL = 'sentinel'
    CLUSTER = 'cluster'


class RedisConfig(BaseSettings):
    mode: RedisClientMode = RedisClientMode.STANDALONE

    # mode: standalone
    url: str = ''                   # redis://localhost:6379

    # mode: sentinel
    sentinels: str = ''             # 192.168.0.1:6001,192.168.0.2:6001
    sentinel_master: str = ''       # master

    # mode: cluster:
    clusters: str = ''              # 192.168.0.1:6001,192.168.0.2:6001

    # mode: sentine/cluster
    urls: str = ''                  # redis://192.168.0.1:6379,redis://192.168.0.2:6379

    def get_sentinels(self):
        if self.sentinels:
            return parse_nodes(self.sentinels)
        return self.sentinels

    def get_clusters(self):
        if self.clusters:
            return [
                {'host': host, 'port': port}
                for host, port in parse_nodes(self.clusters)
            ]
        return self.clusters

    def get_urls(self):
        return self.urls.split(',')
