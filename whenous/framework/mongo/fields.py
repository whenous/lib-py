from functools import partial
from pydantic import Field


__all__ = ('_IdField', )


_IdField: Field = partial(Field, alias='_id')


