from typing import Type
from functools import lru_cache
from motor.motor_asyncio import AsyncIOMotorClient
from .config import MongoConfig


@lru_cache
def get_client(config_class: Type[MongoConfig] = MongoConfig) -> AsyncIOMotorClient:
    conf: MongoConfig = config_class()
    return AsyncIOMotorClient(
        conf.url,
        connectTimeoutMS=conf.connect_timeout_ms,
        socketTimeoutMS=conf.socket_timeout_ms,
        serverSelectionTimeoutMS=conf.server_selection_timeout_ms
    )


class DBManager:

    @property
    def client(self) -> AsyncIOMotorClient:
        if not self._client:
            self._client = get_client()
        return self._client
