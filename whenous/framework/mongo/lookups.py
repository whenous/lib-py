from typing import Dict, Optional, Type


__all__ = ('Lookups', )

_lookup_map = {}


class Lookup:
    __slots__ = ('operator',)

    operator: str

    def __init_subclass__(cls) -> None:
        super().__init_subclass__()
        _lookup_map[cls.operator] = cls

    @classmethod
    def query(cls, key, value):
        return {key: {f'${cls.operator}': value}}


##############################################################################
# Comparison
##############################################################################


class Eq(Lookup):
    operator = 'eq'


class EqAlias(Lookup):
    operator = ''

    @classmethod
    def query(cls, key, value):
        return {key: value}


class In(Lookup):
    operator = 'in'


class Nin(Lookup):
    operator = 'nin'


class Gt(Lookup):
    operator = 'gt'


class Lt(Lookup):
    operator = 'lt'


class Gte(Lookup):
    operator = 'gte'


class Lte(Lookup):
    operator = 'lte'


class Ne(Lookup):
    operator = 'ne'


class Regex(Lookup):
    operator = 'regex'

##############################################################################
# Element
##############################################################################


class Exists(Lookup):
    operator = 'exists'


##############################################################################
# Array
##############################################################################


class All(Lookup):
    operator = 'all'


class Size(Lookup):
    operator = 'size'


##############################################################################
# TextSearch
##############################################################################


class TextSearch(Lookup):
    operator = 'textsearch'

    @classmethod
    def query(cls, key, value):
        return {'$text': {'$search': value}}


##############################################################################
# Lookups
##############################################################################


class Lookups:

    @staticmethod
    def match(operator: str) -> Optional[Type[Lookup]]:
        return _lookup_map.get(operator)

    @staticmethod
    def default():
        return EqAlias


if __name__ == '__main__':
    print(_lookup_map)
