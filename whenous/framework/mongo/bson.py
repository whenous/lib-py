from bson import ObjectId
from pydantic.json import ENCODERS_BY_TYPE


def patch_object_id():
    """ Add support For ObjectId to adapt pydantic """

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not cls.is_valid(v):
            raise TypeError('ObjectId required')
        return cls(v)

    @classmethod
    def __modify_schema__(cls, field_schema: dict):
        field_schema['type'] = 'string'

    ObjectId.__get_validators__ = __get_validators__
    ObjectId.validate = validate
    ObjectId.__modify_schema__ = __modify_schema__
    ENCODERS_BY_TYPE[ObjectId] = str

patch_object_id()


