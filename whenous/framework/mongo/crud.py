from dataclasses import is_dataclass, asdict
from typing import (
    Dict,
    Sequence,
    List,
    Tuple,
    Union,
    TypeVar,
    ClassVar,
    Generic,
    Type,
    get_origin,
    get_args
)
from bson import ObjectId
from pydantic import BaseModel
from pydantic.dataclasses import dataclass
from motor.motor_asyncio import (
    AsyncIOMotorClient,
    AsyncIOMotorCollection,
    AsyncIOMotorClientSession
)


__all__ = ('CRUD', )


ModelType = TypeVar('ModelType', BaseModel, dataclass)
IdType = Union[str, ObjectId]
DocType = Union[BaseModel, dataclass, dict]


class CRUD(Generic[ModelType]):
    """ Mongo CRUD
    usage:
        class Test(BaseModel):
            id: int

        class TestCRUD(CRUD[Test]):
            __db__ = 'test'
            __collection__ = 'test'

        crud = TestCRUD()
        crud.get_by_id()
    """

    __db__: ClassVar[str]
    __collection__: ClassVar[str]

    def __init__(self):
        self._client: AsyncIOMotorClient = None
        self._collection: AsyncIOMotorCollection = None
        self._session: AsyncIOMotorClientSession = None

    @property
    def client(self) -> AsyncIOMotorClient:
        return self._client

    @client.setter
    def client(self, client: AsyncIOMotorClient):
        self._client = client

    @property
    def collection(self) -> AsyncIOMotorCollection:
        if not self._collection:
            db = self.client[self.__db__]
            self._collection = db[self.__collection__]
        return self._collection

    @property
    def session(self) -> AsyncIOMotorClientSession:
        return self._session

    @session.setter
    def session(self, session: AsyncIOMotorClientSession):
        self._session = session

    @property
    def model(self) -> Type[ModelType]:
        """ 获取Generic定义的类型
        https://stackoverflow.com/questions/57706180/generict-base-class-how-to-get-type-of-t-from-within-instance
        """
        for kls in (getattr(self, '__orig_class__', None),) + getattr(self, '__orig_bases__', tuple()):
            if get_origin(kls) is not CRUD:
                continue
            model = get_args(kls)[0]
            return model

    @staticmethod
    def _parse_doc(document: DocType) -> dict:
        if isinstance(document, BaseModel):
            return document.dict(by_alias=True)
        elif is_dataclass(document):
            return asdict(document)
        return document

    # @staticmethod
    # def _patch_id(documents: List[DocType], ids: List[ObjectId]):
    #     for idx, doc in enumerate(documents):
    #         _id = ids[idx]
    #         if isinstance(doc, dict):
    #             doc['_id'] = _id
    #         elif isinstance(doc, BaseModel):
    #             doc._id = _id

    ##########################################################################
    # base crud
    ##########################################################################

    async def find_one(self, query: dict, projection: list = None) -> ModelType:
        r = await self.collection.find_one(query, projection, session=self.session)
        if not r:
            return r
        return self.model(**r)

    async def find_many(self, query: dict, projection: list = None, skip: int = None, limit: int = None, sort: Sequence = None) -> List[ModelType]:
        cursor = self.collection.find(query, projection, session=self.session)
        if skip:
            cursor = cursor.skip(skip)
        if limit:
            cursor = cursor.limit(limit)
        if sort:
            cursor = cursor.sort(sort)
        rs = await cursor.to_list(None)
        return [self.model(**r) for r in rs]

    async def insert_one(self, document: DocType) -> ObjectId:
        doc = self._parse_doc(document)
        r = await self.collection.insert_one(doc, session=self.session)
        return r.inserted_id

    async def insert_many(self, documents: List[DocType]) -> List[ObjectId]:
        docs = [self._parse_doc(r) for r in documents]
        r = await self.collection.insert_many(docs, session=self.session)
        return r.inserted_ids

    async def delete_many(self, query: dict):
        return await self.collection.delete_many(query, session=self.session)

    async def delete_one(self, query: dict):
        return await self.collection.delete_one(query, session=self.session)

    async def update_many(self, query: dict, update: DocType):
        doc = self._parse_doc(update)
        return await self.collection.update_many(query, {'$set': doc}, session=self.session)

    async def update_one(self, query: dict, update: DocType):
        doc = self._parse_doc(update)
        return await self.collection.update_one(query, {'$set': doc}, session=self.session)

    async def upsert_many(self, query: dict, update: Dict[str, DocType]):
        doc = {}
        for key, val in update.items():
            doc[key] = self._parse_doc(val)
        return await self.collection.update_many(query, doc, upsert=True, session=self.session)

    async def upsert_one(self, query: dict, update: Dict[str, DocType]):
        doc = {}
        for key, val in update.items():
            doc[key] = self._parse_doc(val)
        return await self.collection.update_one(query, doc, upsert=True, session=self.session)

    async def count(self, query: dict) -> int:
        return await self.collection.count_documents(query, session=self.session)

    # def aggregate(self, pipeline):
    #     return self.collection.aggregate(pipeline)

    # def distinct(self, key, query=None):
    #     return self.collection.distinct(key, query)

    ##########################################################################
    # biz crud
    ##########################################################################

    async def get_by_id(self, _id: IdType, projection: list = None) -> ModelType:
        _id = ObjectId(_id)
        return await self.find_one({'_id': _id}, projection)

    async def get_list(self, query: dict, projection: list = None, skip: int = None, limit: int = None, sort: Sequence = None) -> Tuple[int, List[ModelType]]:
        count = await self.count(query)
        rs = await self.find_many(query, projection, skip, limit, sort)
        return count, rs

    async def update_by_id(self, _id: IdType, update: DocType):
        _id = ObjectId(_id)
        return await self.update_one({'_id': _id}, update)

    async def delete_by_id(self, _id: IdType):
        _id = ObjectId(_id)
        return await self.delete_one({'_id': _id})
