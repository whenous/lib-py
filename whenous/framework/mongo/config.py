from whenous.framework.pydantic.settings import BaseSettings


class MongoConfig(BaseSettings):
    url: str
    connect_timeout_ms: int = 2000
    socket_timeout_ms: int = 2000
    server_selection_timeout_ms: int = 2000

    class Config:
        env_prefix = 'mongo_'
