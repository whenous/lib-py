from fastapi import APIRouter
from fastapi.responses import Response


router = APIRouter()


@router.get("/ping")
async def ping():
    return Response("OK")
