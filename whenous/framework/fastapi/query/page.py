from typing import Generic, TypeVar, List
from pydantic.generics import GenericModel


__all__ = ('Page', )


ModelType = TypeVar('ModelType')


class Page(GenericModel, Generic[ModelType]):
    total: int
    page: int
    page_size: int
    data: List[ModelType]
