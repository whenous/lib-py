from pydantic import BaseModel
from whenous.framework.mongo.lookups import Lookups


__all__ = ('MongoQuery',)


class BaseQuery(BaseModel):
    """
    usage:
        class SomeQuery(Queries):
            type: str = Query('hello')
            id: Optional[int] = Query(None, alias='_id')
            group_id: Optional[int] = Query(None, nullable=True)
            not_in_db: str = Query('not_in_db', manual=True)

        @app.get('/')
        def index(query: SomeQuery = Depends()):
            q = query.build()

        GET /?_id=1&
        # {'type': 'hello', '_id': 1, 'group_id': None}
    """

    @staticmethod
    def parse_lookup(key, value) -> dict:
        """
        :param key:     item__id__eq
        :param value:   1
        :return:    {'item.id': 1}
        """
        raise NotImplementedError()

    def build(self) -> dict:
        query = {}
        for key, value in self.__dict__.items():
            field = self.__fields__.get(key)
            if not field:
                continue
            extra = field.field_info.extra
            if extra.get('manual'):
                continue
            if value is None and not extra.get('nullable'):
                continue
            name = field.alias or key
            parse_lookup = self.parse_lookup
            if extra.get('parse_lookup'):
                if isinstance(extra['parse_lookup'], str):
                    parse_lookup = getattr(self, extra['parse_lookup'])
                else:
                    parse_lookup = extra['parse_lookup']
            sub = parse_lookup(name, value)
            query.update(sub)
        return query


class MongoQuery(BaseQuery):

    @staticmethod
    def parse_lookup(key, value) -> dict:
        keys = key.split('__')
        operator = keys[-1]
        lookup = Lookups.match(operator)
        if lookup:
            return lookup.query(
                key='.'.join(keys[:-1]),
                value=value
            )
        return Lookups.default().query(key, value)

