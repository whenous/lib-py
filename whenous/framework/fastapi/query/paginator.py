from fastapi import Query
from pydantic import BaseModel


__all__ = ('Paginator',)


class Paginator(BaseModel):
    page: int = Query(default=1, ge=1)
    page_size: int = Query(default=10, le=100)

    @property
    def skip(self):
        return (self.page - 1) * self.page_size

    @property
    def limit(self):
        return self.page_size
