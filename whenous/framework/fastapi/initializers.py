import importlib
from typing import Dict, List, Union, Protocol
from fastapi import FastAPI


__all__ = (
    'init_routers',
    'init_middlewares',
    'init_exception_handlers',
    'Initializer'
)


#################################################################################
# Utils
#################################################################################


def import_object(path):
    ''' import object
    such as: apps.translate.controllers.router
    '''
    module, func = path.rsplit('.', 1)
    mod = importlib.import_module(module)
    return getattr(mod, func)


#################################################################################
# Router
#################################################################################


def init_routers(app: FastAPI, routers: List[Union[str, dict]]):
    """ 初始化路由
    :param routers:
        1. ["apps.translate.controllers.router"]
        2. [{'router': '"apps.translate.controllers.router"'}]
    """
    for rt in routers:
        router = rt
        kwargs = {}
        if isinstance(rt, dict):
            kwargs = rt.copy()
            router = kwargs.pop('router')
        app.include_router(import_object(router), **kwargs)


#################################################################################
# Middleware
#################################################################################


def init_middlewares(app: FastAPI, middlewares: list):
    """
    初始化http middleware
    https://fastapi.tiangolo.com/tutorial/middleware/

    :middlewares: [{"middleware": "utils.fastapi.middlewares.TraceIDMiddleware", "options": {}}]
    """
    for mid in middlewares:
        pkg = mid['middleware']
        options = mid.get('options', {})
        mdw = import_object(pkg)
        app.add_middleware(mdw, **options)


#################################################################################
# Exception Handlers
#################################################################################


def init_exception_handlers(app: FastAPI, exception_handler_map: dict):
    """ 初始化异常处理
    :param exception_handler_map: {'fastapi.exceptions.RequestValidationError': 'handlers.some_handler'}
    """
    for excpetion_name, handler_name in exception_handler_map.items():
        if isinstance(excpetion_name, str):
            excpetion = import_object(excpetion_name)
        else:
            excpetion = excpetion_name      # maybe status code
        handler = import_object(handler_name)
        app.add_exception_handler(excpetion, handler)

#################################################################################
# Config
#################################################################################


class BaseAppConfig(Protocol):
    ROUTERS: List[str] = [
        'whenous.framework.fastapi.controllers.ping.router',
    ]
    EXCEPTION_HANDLERS: Dict[str, str] = {
        'fastapi.exceptions.HTTPException': 'whenous.framework.fastapi.exceptions.http_exception_handler',
        'fastapi.exceptions.RequestValidationError': 'whenous.framework.fastapi.exceptions.request_validation_exception_handler',
        'whenous.framework.fastapi.exceptions.BizError': 'whenous.framework.fastapi.exceptions.biz_error_handler'
    }
    MIDDLEWARES: List[str] = []


#################################################################################
# Initializer
#################################################################################

class Initializer:

    @staticmethod
    def new(config: BaseAppConfig) -> FastAPI:
        app = FastAPI()
        if config.ROUTERS:
            init_routers(app, config.ROUTERS)
        if config.EXCEPTION_HANDLERS:
            init_exception_handlers(app, config.EXCEPTION_HANDLERS)
        if config.MIDDLEWARES:
            init_middlewares(app, config.MIDDLEWARES)
        return app



