
from fastapi.requests import Request
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import HTTPException
from fastapi.exceptions import RequestValidationError
from starlette.status import HTTP_422_UNPROCESSABLE_ENTITY
from .exception import BizError


__all__ = (
    'http_exception_handler',
    'request_validation_exception_handler',
    'biz_error_handler'
)


async def http_exception_handler(request: Request, exc: HTTPException) -> JSONResponse:
    headers = getattr(exc, "headers", None)
    content = {'code': -1, "msg": 'http error', 'data': exc.detail}
    return JSONResponse(
            content,
            status_code=exc.status_code,
            headers=headers
        )


async def request_validation_exception_handler(
    request: Request, exc: RequestValidationError
) -> JSONResponse:
    content = {'code': -1, 'msg': 'validation error', 'data': jsonable_encoder(exc.errors())}
    return JSONResponse(
        content,
        status_code=HTTP_422_UNPROCESSABLE_ENTITY,
    )


async def biz_error_handler(
    request: Request, exc: BizError
) -> JSONResponse:
    content = {'code': exc.code, 'msg': exc.msg, 'data': exc.data}
    return JSONResponse(
        content,
        status_code=exc.status_code,
    )



