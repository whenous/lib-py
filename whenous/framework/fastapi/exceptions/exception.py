
from typing import Any
from starlette import status


class BizError(RuntimeError):

    def __init__(self, code: int = -1, msg: str = '', data: Any = None, status_code: int = status.HTTP_200_OK):
        self.code = code
        self.msg = msg
        self.data = data
        self.status_code = status_code
        super().__init__()
