from bson import ObjectId
from fastapi import Request, Response
from starlette.middleware.base import BaseHTTPMiddleware, RequestResponseEndpoint


__all__ = ('TraceIDMiddleware', )


class TraceIDMiddleware(BaseHTTPMiddleware):
    """
    追踪id (trace id)
    """
    KEY = 'x-tid'

    async def dispatch(
        self, request: Request, call_next: RequestResponseEndpoint
    ) -> Response:
        response = await call_next(request)
        if not request.cookies.get(self.KEY):
            val = str(ObjectId())
            response.set_cookie(self.KEY, val)
        return response
