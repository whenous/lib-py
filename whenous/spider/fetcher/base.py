import abc
from typing import Optional, Protocol
from dataclasses import dataclass, field
from whenous.utils.function.retry import AsyncRetry


@dataclass
class FetchConfig:
    headers: dict = field(default_factory=lambda: {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36'
    })
    cookies: dict = field(default_factory=lambda: {})
    allow_redirects: bool = False

    timeout: int = 5
    retry_delay: int = 10
    retry_max_times: int = 3


class FetchResponse(Protocol):
    status_code: int
    content: str
    text: str
    headers: dict
    cookies: dict


@dataclass
class FetchResult:
    response: Optional[FetchResponse] = None
    error: Optional[Exception] = None


class AsyncFetcher(abc.ABC):

    def __init__(self, config: FetchConfig) -> None:
        self.config = config

    @AsyncRetry(max_factory=lambda self: self.config.retry_max_times, retry_factory=lambda _, result: bool(result.error))
    async def fetch(self, url) -> FetchResult:
        return await self._fetch(url)

    @abc.abstractmethod
    async def _fetch(self, url) -> FetchResult:
        pass
