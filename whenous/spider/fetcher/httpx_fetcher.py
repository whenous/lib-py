import typing
import httpx
from .base import AsyncFetcher, FetchConfig, FetchResult


U = typing.TypeVar("U", bound="HttpxFetcher")


class HttpxFetcher(AsyncFetcher):

    def __init__(self, config: FetchConfig) -> None:
        super().__init__(config)
        self.client = httpx.AsyncClient(
            headers=config.headers,
            cookies=config.cookies,
            timeout=config.timeout,
        )

    async def __aenter__(self: U) -> U:
        return self

    async def __aexit__(self, *args):
        await self.client.aclose()

    async def _fetch(self, url) -> FetchResult:
        result = FetchResult()
        try:
            resp = await self.client.get(url, allow_redirects=self.config.allow_redirects)
            result.response = resp
        except (httpx.TimeoutException, httpx.NetworkError, httpx.StreamError) as err:
            result.error = err
        except Exception as err:
            result.error = err
        return result
